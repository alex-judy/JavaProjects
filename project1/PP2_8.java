/*
Write a program that reads values representing the weight in
kilograms, grams, and milligrams and then prints the equivalent
weight in milligrams. (For example, 1 kilogram, 50 grams, and
42 milligrams is equivalent to 10,50,042 milligrams.)
*/

package project1;

import java.util.Scanner;

public class PP2_8 
{

	public static void main(String[] args)
	{
		int kilo, gram, milli, total;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.print("Enter number of kilograms: ");
		kilo = scan.nextInt();
		
		System.out.print("Enter number of grams: ");
		gram = scan.nextInt();
		
		System.out.print("Enter number of milligrams: ");
		milli = scan.nextInt();
		
		scan.close();
		
		total = ((kilo * 1000000) + (gram * 1000) + milli);
		
		System.out.println("The number of milligrams after conversion is: " + total);
	}
}
