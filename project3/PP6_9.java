package project3;

import java.util.*;

public class PP6_9 {
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int countA = 0, countE = 0, countI = 0, countO = 0, countU = 0, countNonVow = 0;
		String sentence;
		
		System.out.print("Please enter your sentence: ");
		sentence = scan.nextLine();
		
		for (int i = 0; i < sentence.length(); i++)
		{
			if (sentence.charAt(i) == 'a' || sentence.charAt(i) == 'A')
				countA ++;	

			else if (sentence.charAt(i) == 'e' || sentence.charAt(i) == 'E')
				countE ++;

			else if (sentence.charAt(i) == 'i' || sentence.charAt(i) == 'I')
				countI ++;

			else if (sentence.charAt(i) == 'o' || sentence.charAt(i) == 'O')
				countO ++;

			else if (sentence.charAt(i) == 'u' || sentence.charAt(i) == 'U')
				countU ++;

			else 
				countNonVow ++;
		}
		
		System.out.println("A: " + countA + "\nE: " + countE + "\nI: " + countI + "\nO: " + countO + "\nU: " + countU + "\nNonvowel: "
				+ countNonVow);

		scan.close();
	}

}
