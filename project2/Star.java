/*Create a class called Star that represents a five-pointed star. Let the constructor
accept the width of the star and the coordinates of the star�s top point. Write a
program that draws a sky containing stars of various sizes.
*/

package project2;

import java.awt.*;

public class Star
{
    private int width;
    private int x;
    private int y;
    private int x1, x2, x3, x4, x5;
    private int y1, y2, y3, y4, y5;

    public Star(int width, int x, int y){
    	this.width = width;
    	this.x = x;
    	this.y = y;
    }
    
    public void setX(int x){
    	this.x = x;
    }
    
    public void setY(int y){
    	this.y = y;
    }
    public void setWidth(int width){
    	this.width = width;
    }
    
    public int getX(){
    	return x;
    }
    
    public int getY(){
    	return y;
    }
    
    public int getWidth(){
    	return width;
    }
    
    public void drawStar(Graphics page){
    	
    	//Make star using drawLine
    	
    	
    	x1 = x + width / 2; //Takes x coordinate plus half of the width. This centers the top point
    	y1 = y; //Take original y coordinate
    	x2 = x1 + width / 2; //Add half of width to next point
    	y2 = (int) ((double) (y) + .374 * (double) (width));
    	x3 = (int) ((double) (x) + .825 * (double) (width));
    	y3 = y + width; //Adding width to y makes the size proportional 
    	x4 = (int) ((double) (x) + .175 * (double) (width));
    	y4 = y + width; //Adding width to y sizes makes the size proportional 
    	x5 = x; //Take original x coordinate
    	y5 = y2;
    	
    	page.drawLine(x1, y1, x3, y3);
    	page.drawLine(x2, y2, x4, y4);
    	page.drawLine(x3, y3, x5, y5);
    	page.drawLine(x4, y4, x1, y1);
    	page.drawLine(x5, y5, x2, y2);
    	
    	
    	
    	/*						Star Coordinate Plan		
    	  				                (x1, y1)						
    	
    	
    					(x5, y5)						(x2, y2)
    	
    	
    	
    	
    						(x4, y4)				(x3, y3)
    	
    	*/
    	
    }
}	